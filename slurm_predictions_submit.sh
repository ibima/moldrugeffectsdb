#!/bin/bash

set -e

Rscript=cmap_predictors_assessment_1_runInQueue.R 
Workdir=/ngs/sm718/moldrugeffectsdb

if [ -z "$Time" ]; then
   Time="2-12"
   echo "I: Setting slurm Time to '$Time'"
fi

if [ -z "$Mem" ]; then
   Mem=27000
   echo "I: Setting slurm Mem(ory) to '$Mem'"
fi

if [ -z "$Account" ]; then
   Account=cluster
   echo "I: Setting slurm Account to '$Account'"
fi
if [ -z "$Partition" ]; then
   Partition=interactive
   echo "I: Setting slurm Partition to '$Partition'"
else
   echo "I: Using suggested partition '$Partition'"
fi

if [ -n "$1" ]; then

   if [ "-h" = "$1" -o "--help" = "$1" -o "-help" = "$1" -o "help" = "$1" ]; then
      cat <<EOHELP
Usage: $(basename $0) [jobnames for predictions to run]
       $(basename $0) --list
       $(basename $0) --help

This script supports the submission of jobs to _perform_ a particular
_prediction_ on a SLURM cluster.  Hereto it wraps the R script
"$Rscript".
The communication of the command line with that R script is performed via
envrionment variables. Especially the there is the environment variable
CMAP_JOBS that defines what is to be run.

The mapping from these job names to functions to be executed
is performed in the R structure "predictions.jobs" from the file
"cmap_predictors_assessment_0_run_common.R".  If started parameters,
the arguments to this script are interpreted as job names.  If started
without parameters, the values of CMAP_JOBS environment variable are
passed on.  If that variable is unset, then a list of jobs for which
output files are missing in "interim/predictions/" will be shown.

Parameters:
  Rscript=$Rscript
  Workdir=$Workdir
  Time=$Time
  Mem=$Mem
  Partition=$Partition
  Account=$Account
  Nodes=$Nodes
EOHELP
      exit
   fi

   if [ "-l" = "$1" -o "--list" = "$1" -o "-list" = "$1" -o "list" = "$1" ]; then
      CMAP_JOBS="" R --quiet --vanilla < $Rscript
      exit
   fi

   cd $Workdir
   if [ ! -r "$Rscript" ]; then
      echo "E: Cannot find R script '$Rscript' to execute"
      echo "   $(pwd)"
      exit 1
   fi

   if [ -n "$Nodes" ]; then
      Nodes="-w $Nodes"
   fi

   for i in $*
   do
      export CMAP_JOBS="$i"
      #workdir=$(dirname $0)
      echo "I: submitting with Workdir=$Workdir"
      echo "I: submitting with Rskript=$Rscript"


      line="CMAP_JOBS=$CMAP_JOBS sbatch --account=$Account --partition $Partition --time=$Time --chdir=$Workdir --cpus-per-task=1 --job-name=CMAP_$i -n 1 --cores-per-socket=1 --threads-per-core=1 --output=slurm_output_cmap_$i.txt --mem=$Mem $Nodes ./$(basename $0)"
      echo $line
      CMAP_JOBS=$CMAP_JOBS sbatch --account=$Account --partition $Partition --time=$Time --chdir=$Workdir --cpus-per-task=1 --job-name=CMAP_$i -n 1 --cores-per-socket=1 --threads-per-core=1 --output=slurm_output_cmap_$i.txt --mem=$Mem $Nodes ./$(basename $0)
   done
else
   if [ "" = "$CMAP_JOBS" ]; then
      echo "E: CMAP_JOBS environment variable not set"
      exit 1
   fi
   if [ ! -r "$Rscript" ]; then
      echo "E: Cannot find R script '$Rscript' to execute"
      echo "   $(pwd)"
      exit 1
   fi
   env
   echo "CMAP_JOBS=$CMAP_JOBS"
   echo "# date: $(date -R)"
   echo "# host: $(hostname)"
   echo "# pwd:  $(pwd)"
   R --quiet --vanilla < $Rscript
   echo "# date: $(date -R)"
   echo "end"
fi
