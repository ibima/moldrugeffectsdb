lincs<-NULL
fpath <- "inst/extdata/lincs_landmark_genes.tsv"
if (!file.exists(fpath)) {
   fpath <- system.file("extdata", "lincs_landmark_genes.tsv", package="MolDrugEffectsDB")
   if (!file.exists(fpath)) {
      fpath <- NULL
   }
}
if (is.null(fpath)) {
  stop("E: Could not find lincs_landmark_genes.tsv")
}

lincs.lm.table <- read.delim(fpath,sep="\t",header=T,stringsAsFactors=F);
lincs.lm.genes <- lincs.lm.table[,1]
rownames(lincs.lm.table)<-lincs.lm.table[,1]

# Last executed 8/2018
require(biomaRt)
ensembl <- useMart("ensembl",dataset="hsapiens_gene_ensembl")
results <- getBM(attributes=c('hgnc_symbol','entrezgene'), 
                 filters = 'hgnc_symbol', 
                 values = lincs.lm.genes, 
                 mart = ensembl)

missing.genes <- lincs.lm.genes[!lincs.lm.genes %in% results[,1]]
lincs.lm.table[missing.genes]

# manually assigned via https://www.ncbi.nlm.nih.gov/gene
missing.table <- matrix(c(
 "TOMM70A",  "9868",
 "KIAA0196", "9897",
 "KIAA0907", "22889",
 "PAPD7",    "11044",
 "IKBKAP",   "8518",
 "TMEM5",    "10329",
 "HDGFRP3",  "50810",
 "PRUNE",    "58497",
 "HN1L",     "90861",
 "KIAA1033", "23325",
 "TMEM110",  "375346",
 "SQRDL",    "58472",
 "TMEM2",    "23670",
 "ADCK3",    "56997",
 "LRRC16A",  "55604",
 "FAM63A",   "55793"
),ncol=2,byrow=T,dimnames=list(NULL,c("hgnc_symbol","entrez")))

rownames(missing.table) <- missing.table[,1]

r <- cbind("hgnc_symbol"=results[,1],
           "entrez"=as.character(results[,2]))
rownames(r) <- r[,1]

lincs.lm.table.entrez <- rbind(r,missing.table)

save(file="data/lincsLandmarkEntrez.RData",lincs.lm.table.entrez)
