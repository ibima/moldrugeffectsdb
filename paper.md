---
title: 'Processing of Connectivity Map .CEL files for investigating drug effects'
tags:
  - drug repositioning
  - transcriptome screening
  - drug effects
  - R
authors:
 - name: Mathias Ernst
   orcid: 0000-0003-1096-4316
   affiliation: 1,2
 - name: Marcin Siatkowski
   orcid: 0000-0000-0000-0000
   affiliation: 1
 - name: Sarah Fischer
   orcid: 0000-0002-6218-7275
   affiliation: 1
 - name: Georg Fuellen
   orcid: 0000-0001-7887-5286
   affiliation: 1
 - name: Steffen Möller
   orcid: 0000-0002-7187-4683
   affiliation: 1
 - name: Stephan Struckmann
   orcid: 0000-0002-8565-7962
   affiliation: 1,3
affiliations:
 - name: Institute for Biostatistics and Informatics in Medicine and Ageing Research, University of Rostock, Germany
   index: 1
 - name: Department of Biology, University of Erlangen, Germany
   index: 2
 - name: Community Medicine, University Medicine Greifswald, Germany
   index: 3
date: 05 October 2017
bibliography: paper.bib
nocite: |
 @entrez, @fitPLM, @nuse, @gplots, @harrison
---

# Summary

The Connectivity Map (CMap) describes the effect of 1309 drugs in
three concentrations on the transcriptomes of up to four different
cell lines. This 2006 effort has become a much respected source to
support the general understanding of a drug's pharmacogenomic effects
as well as models for drug repositioning on orphan diseases [@CMap].

Based on a complete reanalysis of the Affymetrix CEL files of the
project, this R script provides fold changes and absolute RMA
expression levels [@affy] in big matrices.  Batch effects are addressed
with ComBat [@sva]. Once executed, the data is available as an R package [@R].

Users of the Connectivity Map gain extra freedom on constraining
the search for gene effects to genes with particular importance for
their project. This effort is the first to offer consistent access
to both the abundance of transcripts and their fold change upon the
exposure to drugs. It is also the first Open Source protocol post-process
the data, allowing to extend or customize this resource for local needs.

# References
