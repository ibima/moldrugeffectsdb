# Drug Effects described in Connectivity Map CEL files facilitate drug repositioning #

## Summary ##

The Connectivity Map (CMap) describes the effect of 1309 drugs in
three concentrations on the transcriptomes of up to four different
cell lines. This 2006 effort has become a much respected source to
support the general understanding of a drug's pharmacogenomic effects
as well as models for drug repositioning on diseases[^1,2].

Based on a complete reanalysis of the Affymetrix CEL files of the
CMap project, we introduce a protocol implemented in R
to provide fold changes and absolute RMA expression levels as
matrices. Batch effects are addressed with ComBat. Once executed,
the data is available as an R package.

Users of the Connectivity Map gain extra freedom on constraining
the search for gene effects to genes with particular importance for
their project as this Open Source effort is the first to offer
consistent access to both the abundance of transcripts and their
fold change in cells upon their exposure to drugs.


## Introduction ##

The developers of the CMap offer their own online service
(https://portals.broadinstitute.org/cmap/),
but constrain the expressiveness of queries by researchers.
A BioConductor package
(https://bioconductor.org/packages/devel/data/experiment/vignettes/cMap2data/inst/doc/cMap2data.pdf)
offers access to the data, but is explicitly not making available the
R code from which that package was generated, leaving no room
for inspection, criticism or even changes to the post-processing.

Our work is the first to openly invite users to adapt the Connectivitiy
Map data to match their individual routine:

 * mapping of probes to gene names, 
 * dealing with batch effects, 
 * assessment of the reliability of an observed change in expression.

All annotation required for the above is in the description of the samples
and the corresponding .CEL files.
Only on this lowest level of the data, the assignment of samples to plates
and larger batches have been prepared together are revealed.

The community yet lacked a straight-forward processing routine
that is independent from microarray databases and provides both
absolute expression levels and fold changes as a well-documented
input for downstream analyses such as machine learning techniques.

## Setup and Execution ##

For a readily usable R package featuring the CMap data, install
directly from this bitbucket repository with

``` .rconsole
library(devtools)
install_bitbucket("ibima/moldrugeffectsdb",ref="masterWithData")
data(CMap)
```
This transfers >500MB of data, which will take its time.
The remainder of this section describes the process to create
this R package with the CMap raw files offered on the ConnectivityMap
website.

1) Download the experiment description file 'cmapi\_instances\_02.csv' from
the Connectivity Map portal at https://portals.broadinstitute.org/cmap/.
Also download the zipped CEL files.
Uncompressed, these demand 39 GB of disk space.

![Source of raw data and their annotation](inst/doc/cmapDownload.jpg)

*Figure [Download]: After logging into the CMap portal, the download
tab offers four different files. The .CEL files and the spreadsheet with
their description  is needed for the processing, as marked by a green dot.*

2) Install R packages affy, affyPLM, sva, preprocessCore, hthgu133a.db,
hgu133a.db and hgu133a2.db as instructed on https://www.bioconductor.org/install/ .
Also install rentrez for the used of PubChem chemical identifiers.
The *cmap\_build\_deps.R* script automates this preparation.

3) Execute the script cmap.R once to create the file data/CMap.RData. Alternatively,
check out the branch "masterWithData" to receive that 1.5GB file readily prepared.
Then install the
R package with `R CMD INSTALL .` . From within the R shell, load
the package with `library(CMapDrugEffects)` and run `data(CMap)` to access
the list of log fold changes and the absolute means of log expression
levels.

Figures shown were created with cmap\_qa.R,
which may serve as an example on how to use the data provided.

## Methods ##

All computation was performed with R[^3] on Debian Linux (www.debian.org).
Processing comprised the processing of Affymetrix chips with fitPLM[^4]
RMA[^5].
Quality was assessed with NUSE[^6].
Batch effects were
addressed with ComBat[^7], followed by a quantile normalisation. Figures were
created with the support of gplots[^8] as motivated by Harrison[^9].

## Examples ##

For each of the three Affymetrix chip technologies applied in the CMap project, the script yields two
large matrices, both with genes as rows and drugs (concentration, cell line)
as columns. The first matrix offers average log fold changes of
transcripts in response to a drug in a particular cell line. The second
matrix provides the absolute expression levels as shown in Table [Fragment
of resulting table]. The data size is 581MB.

The file *cmap\_qa.R* offers the code samples that produced the tables
and figures here, demonstrating the routines we developed for
performing post-processing that can be found in the file *cmap.R*.

*Table [Platform overview]: Chip platforms used in the project
differ in their coverage of cell types and the number of concentrations
tested per drug and cell type.*

| Platform    | #Genes |#Drugs | Cell types                          | #Combinations |
|-------------|--------|---------------------------------------------|---------------|
| U133AAofAv2 |  12437 |    86 | MCF7 (94)                           |            94 |
| HG-U133A    |  12438 |   295 | HL60 (286), PC3 (74)                |           360 |
| HT_HG-U133A |  12438 |  1141 | HL60 (731), MCF7 (1157), PC3 (1053) |          2941 |

The platforms HG-U133A and HT\_HG-U133A were used to test 63 drugs for the same PC3 cell line,
for one concentration each. Moreover, trichostatin-A shares a measurement on the HL60 line, employing both platforms.

*Table [Fragment of resulting table]: All data from the CMap is aggregated in a single
list with three elements, which represent the three Affymetrix
chip generations used during the course of the project.
Each element represents the data gathered with the respective platform.
The expression data is offered in both relative and absolute numbers
in a single matrix. The matrix' first dimension separates transcripts
by their ENTREZ ID[^10]. The second dimension shows a denominator [GF:??????] combining
drug name, applied concentration and cell line.
The third dimension indicates if values are
log fold changes or the mean of the log2 data generated with RMA [affy].
The abundance in the controls are computed as absolute values minus the fold change.*

```R
> names(CMap)
[1] "U133AAofAv2"    "HG-U133A"    "HT\_HG-U133A"
> options(width=120)
> CMap[["HT\_HG-U133A"]][1:3,1:3,]
, , data = logFoldChange

            drugs
transcripts 0173570-0000_1e-05_MCF7 0173570-0000_1e-05_PC3 0173570-0000_1e-06_MCF7
       10               -0.16782473            -0.05762062              0.00441358
       100               0.12410079            -0.05892558              0.02678044
       1000              0.04364114            -0.23255420              0.12491267
    
, , data = logMean
    
           drugs
transcripts 0173570-0000_1e-05_MCF7 0173570-0000_1e-05_PC3 0173570-0000_1e-06_MCF7
       10                  4.336199               4.483268                4.508437
       100                 8.604236               7.184521                8.506916
       1000                4.808551               9.209366                4.889822
```


![lFC on same compounds across platforms](inst/doc/plot_lfc_comparison_HG-U133A_vs_HT_HG-U133A.jpg)

*Figure [log Fold Changes across platforms]: Measurements of induced fold changes for the same drugs in the same concentrations but on different platforms are plotted against each other. The X axis shows values for the platform HG-U133A, the Y axis represents the platform HT\_HG-U133A.*

Figure [log Fold Changes across platforms] shows that fold changes of more than 0.5 in either direction are confirmed by measurements on the respective other platform.

![difference in log Fold Change against abundance](inst/doc/plot_delta_lfc_against_abundance_HG-U133A_vs_HT_HG-U133A.jpg)

*Figure [difference in lFC against abundance]: Also this second plot refers to the
subset of drugs that have been investigated by the HG-U133A and HT_HG-U133A
platforms. The difference between the log fold changes is plotted on the Y axis,
the log abundance of the transcript is represented on the X axis. The lighter
colour indicates a higher density of represented transcripts.*

Figure [difference in lFC against abundance] shows that the difference between
log fold changes between platforms is centered around zero, invariant
of the abundance of the transcript.

![Searching for drug similarity](inst/doc/plot_where_is_methylprednisolone.jpg)

*Figure [Searching for drug similarity]: The drug methylprednisolone
(random selection from drugs measured at least twice on one chip and also on a second platform) was measured on platforms HG-U133A (for one cell line) and HT_HG-U133A
(for two cell lines). This diagram displays the performance of several
approaches to retrieve the same drug from the Connectivity Map only
by comparing the respective observed log fold changes. All transcripts are ranked by
their log fold change, of which only the top-X are used for the
comparison. The Y axis indicates the rank of the matched methylprednisolone,
the lower the better.*

*The black line is the control, i.e. the reference methylprednisolone
attempts to find itself, and indeed it is always found first, constituting
the horizontal line at the bottom. The red line is the search for the
second methylprednisolone on the HT_HG-U133A platform, which has a minimum
if around 100 (10^2) transcripts are used for comparison. The blue line takes the same log fold
changes and performs the search on the HG-U133A platform.
The dotted lines result from the further constraint that only the top
25% abundant genes are used for the comparison - other genes are ignored.
In green, the data from the two methylprednisolone experiments is combined [???!!!!]: Only 
transcripts that both times up- or both times down-egulated are
considered for ranking the drugs on the HG-U133A platform.*

Figure [Searching for drug similarity] gives conflicting views.
Firstly, to have only the most abundant transcripts does not
necessarily improve the utility of the similarity score. While performance improves
(slightly) on the other platform, performance gets considerably worse when
searching on data of the same platform. It seems also not generally helpful to
include too many genes. Less than 100 transcripts perfomed best
both within and across platforms, while on the other platform, less
than 10 genes provided a minimum. With the exception of searching
for itself, involving a larger number of genes worsens the result.


### Pearson correlation vs. sum of distances vs. random ###

The similarity or distance of two a drug effects, technically these are vectors of numbers,
can be expressed in many ways. The degree of correlation of the change in expression levels across
genes is an obvious candidate as a score, put as 1-cor to indicate similarity.
And so is the sum of absolute pairwise differences of these values. Figure [Predictors against random] 
shows that the quality of the prediction is better for the two predictors based on real data
than a random assignment.


![Predictors against random](inst/doc/comparisonAgainstRandom.jpeg)



### Performance of random assignments ###

The assessment of the quality of a predictor by the rank of the same drug
in another experiment is not equal for all drugs. A major driver is
outside the distribution of the genes, it is with the fraction of
experiments that cover that drug, i.e. for different cell lines and
concentrations. 

We have created a predictor that creates random rank assignments, repeated
100 times. And for all drugs we have asked what their 5% top rank is, shown
in figure [5% best ranks expected by chance].

![5% best ranks expected by chance](inst/doc/fivePercentBoxplotCMap.jpg)

*Figure [5% best ranks expected by chance]: The figure shows boxplot for each
of the three platforms of the connectivity map. Varying in size, the best ranks
achieved by a random order of compounds varies. Every dot represents a compound/celltype/contration
combination.*


### Performance on external data ###

To assess a predictor on data outside the ConnectivityMap, we have prepared
the setup for extensions from the GeneExpressionOmnibus database. For identifying
the compounds tested across experiments, all names were transcribed to their
Chemical Identifier as assigned in PubChem.

* Example to be shown *

## Comments ##

It is expected that a drug's effect on the transcriptome strongly
depends on the tissue. Figure [Searching for drug similarity]
suggests that also the selection of genes that are most affected by a drug are tissue dependent. Still, the signal is in the data, as seen by the improved performance of the search using the consensus gene list, i.e. genes reacting in the same direction upon stimulation by the drug. Many ways to further integrate the data of the CMap or external information like pathway databases are thinkable and our effort hopefully both facilitates and stimulates this research.

The matrices provide absolute expression levels and fold changes, but not P values. This would require a third matrix and one may argue that any transcript that is not trusted to be effected in its expression shall better not show a log Fold Change at all. It is however not foreseeable how this affects downstream algorithms and so we decided to leave such additional filters to our users.

The presented implementation of a drug search is serving as a tutorial for how the matrices can be used and to demonstrate that there is additional value in the dual presentation of absolute values together with the induced changes. This obviously needs further refinement and additional ideas to process the data. The post-processing of the data presented here is extendable, an umbrella for developing and evaluating better algorithms to propose a drug that induces a particular effect in the transcriptome.
The CMap2data package of the Drug versus Disease project [^11] also offers data
from CMap. However, derived from the presentation in public
microarray databases instead of referring to the original data, and
does not differentiate the information on drug concentrations.

The LINCS (www.lincsproject.org) project has recently been released
by the same group. It offers many more tissues, albeit with only
a tenth of the transcriptome directly measured.
More similar to the given effort is CancerXGene
(www.cancerxgene.org) with a smaller coverage of compounds and a
varying dosage normalized by the effect on the growth of cancer
cells.  While these data can be similarly integrated on a technical
level as yet other platforms and cell lines, it is unclear how
the different semantics with respect to gene coverage and concentrations
should be modeled. We hence see the semantically equivalent integration
of those project as a challenge that should be separated from this
effort.

For the development of new algorithms for transcriptomic drug
profiling with an application on benign cells,
it is of interest to have positive controls. Some algorithms
may even suggest that for applying any transcritome-based filter for
a given disease-tissue the user generates these data for a few compounds
that are in the Connectivity Map as a reference.
Discrepancies between predicted effects and these measurements shall
be fed back to the algorithm to improve all its predictions.
To support this feedback loop
we have identified a few data sets from the GeneExpressionOmnibus
that feature drugs which have also been investigated in the
ConnectivityMap but on non-cancerous tissues. We wholeheartedly
invite the community to help extending this effort as it fits
their local needs and possibly stimulate the upload of these
data to public databases.

## Scripts provided ##

1. Basic
   + cmap.R  - transforms CMap raw data into matrices
   + cmap_build_deps.R - run time dependencies for cmap.R
   + cmap_qa.R - ad hoc analyses of CMap data contributing to this README's figures
2. Predictors
   + cmap_predictors.R - implementation of routines expressing similarity between sets of changed genes and how to assess any such derived ranking
   + cmap_prediction_permutations.R - preparation of random data to compare performance against
   + cmap_nameconversion.R - helper routines to interpret CMAP names for compounds
   + network.R - preparation of network from STRING on protein interaction
   + pc.getCids.by.name.R - routine for mapping drug name to chemical compound ID
3. Evaluation
   + cmap_predictors_assessment_0_run_common.R - specification of which predictions are to be made and assessed
   + cmap_predictors_assessment_1_runInQueue.R
   + cmap_predictors_assessment_1_run.R
   + cmap_predictors_assessment_2_evaluateInQueue.R
   + cmap_predictors_assessment_2_evaluate.R
   + cmap_predictors_assessment_3_plot.R
   + slurm_predictions_submit.sh - execution of prediction in Slurm cluster
   + slurm_assesments_submit.sh - assessment of predictions in Slurm cluster
4. Presentation
   + plot_cmap_predictions.R
5. Extension
   + external.R - preparation of an external dataset

## Acknowledgements ##

This project has received funding from the European Union's Horizon
2020 research and innovation programme under Grant agreement No
633589 and benefited from EU COST "cHiPSet" (IC1406).

## Contact ##

This package was implemented by Mathias Ernst with contributions
by Marcin Siatkowski, Sarah Fischer, Georg Fuellen, Steffen Moeller
and Stephan Struckmann.  Contact Steffen <steffen.moeller at
uni-rostock.de> for questions or create an issue with
https://bitbucket.org/ibima/moldrugeffectsdb/issues.


## References ##

[^1]: Lamb J, Crawford ED, Peck D, Modell JW, Blat IC, Wrobel MJ,
Lerner J, Brunet JP, Subramanian A, Ross KN, Reich M, Hieronymus
H, Wei G, Armstrong SA, Haggarty SJ, Clemons PA, Wei R, Carr SA,
Lander ES, Golub TR. (2006) "The Connectivity Map: using gene-expression
signatures to connect small molecules, genes, and disease." Science.
313(5795):1929-1935.

[^2]: Francesco Iorio, Timothy Rittman, Hong Ge, Michael Menden, Julio Saez-Rodriguez (2013) "Transcriptional data: a new gateway to drug repositioning?" Drug Discov Today. 18(7-8): 350-357. doi:10.1016/j.drudis.2012.07.014

[^3]: R Core Team (2017) "R: A Language and Environment for Statistical Computing" R Foundation for Statistical Computing, Vienna, Austria, https://www.R-project.org

[^4]: Bolstad, BM (2004) "Low Level Analysis of High-density Oligonucleotide Array Data: Background, Normalization and Summarization." Dissertation. University of California, Berkeley.

[^5]: Gautier, L., Cope, L., Bolstad, B. M., and Irizarry, R. A. (2004) "affy---analysis of Affymetrix GeneChip data at the probe level." Bioinformatics 20(3):307-315.

[^6]: Brettschneider J, Collin F, Bolstad BM, and Speed TP. (2007) "Quality assessment for short
  oligonucleotide arrays." Technometrics. In press

[^7]: Jeffrey T. Leek, W. Evan Johnson, Hilary S. Parker, Elana J. Fertig, Andrew E. Jaffe, John D. Storey, Yuqing Zhang and Leonardo Collado Torres (2017) "sva: Surrogate Variable Analysis." R package version 3.24.4.

[^8]: Gregory R. Warnes et al., (2016) "gplots: Various R Programming Tools for Plotting Data" R package version 3.0.1, https://CRAN.R-project.org/package=gplots

[^9]: Myles Harrison (2014) "5 Ways to Do 2D Histograms in R", https://github.com/mylesmharrison/5\_ways\_2D\_histograms

[^10]: McEntyre, J. (1998) "Linking up with Entrez." Trends in Genetics 14(1):39-40.

[^11]: Clare Pacini, Francesco Iorio, Emanuel Goncalves, Murat Iskar, Thomas Klabunde, Peer Bork, Julio Saez-Rodriguez (2013) "DvD: An R/Cytoscape pipeline for drug repurposing using public repositories of gene expression data" Bioinformatics 29(1):132-134. doi:10.1093/bioinformatics/bts656
